## Localization

Localization is done via .json files. Keys are in English and values are in your preferred language.

Please try to preserve formatting, and check to see if the output looks good on a 120 chars wide terminal.
